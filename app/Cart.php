<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public static function getCartArray()
    {
        $cart = json_decode(request()->cookie('cart'), true);
        if(!is_array($cart)){
            $cart = [];
        }
        return $cart;
    }
    public static function addProduct(Product $product)
    {
        $cart = self::getCartArray();

        if (isset($cart[$product->id])){
            $cart[$product->id]++;
        }else{
            $cart[$product->id] = 1;
        }

        return $cart;
    }

    public static function removeProduct(Product $product)
    {
        $cart = self::getCartArray();
        if (isset($cart[$product->id])){
            $cart[$product->id]--;
            if ($cart[$product->id] < 1){
                unset($cart[$product->id]);
            }
        }
        return $cart;
    }

    public static function getCartWithProducts()
    {
        $cart = self::getCartArray();
        $cartWithProducts = [];
        if(count($cart) > 0){
            foreach($cart as $productId => $amount) {
                $cartWithProducts[] = [
                    'amount' => $amount,
                    'product' => Product::find($productId)
                ];
            }
        }
        return$cartWithProducts;
    }
}
