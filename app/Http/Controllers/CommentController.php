<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentsRequest;
use App\Http\Requests\CommentsUpdateRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(CommentsRequest $request)
    {
        Comment::create($request->all() + ['user_id' => Auth::user()->id]);
        return back()->with(['status' => 'Comment created!']);
    }



    public function edit(Comment $comment)
    {
        return view('comments.edit', compact('comment'));
    }


    public function update(CommentsUpdateRequest $request, Comment $comment)
    {
        $comment->update($request->all() + ['user_id' => Auth::user()->id]);
        return redirect('/posts/' . $comment->post->slug)->with(['status' => 'Comment succesfully updated!']);
    }


    public function destroy(Comment $comment)
    {
        $comment->delete();
        return back()->with(['status' => 'Comment succesfully deleted!']);;
    }
}
