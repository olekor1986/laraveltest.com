<?php

namespace App\Http\Controllers;
use App\Http\Requests\PostsRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $posts = Post::all();
        $pageTitle = 'Super Blog Page';
        return view('posts.index', compact('posts', 'pageTitle'));
    }

    public function show(Post $post)
    {
       /*
        *  Delete this
        *  $post = Post::find($post);
        */
        return view('posts.show', compact('post'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(PostsRequest $request)
    {
        $postArray = $request->only(['title', 'slug', 'intro', 'body']);
        $postArray['user_id'] = Auth::user()->id;
        Post::create($postArray);
        return redirect('/posts');
    }
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }
    public function update(PostsRequest $request, Post $post)
    {

        $postArray = $request->only(['title', 'slug', 'intro', 'body']);
        $postArray['user_id'] = Auth::user()->id;
        dd($postArray);
        $post->update($postArray);
        return redirect('/posts');
    }
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect('/posts');
    }
}
