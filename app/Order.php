<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 'phone', 'comment'];

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('amount');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
