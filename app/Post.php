<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //protected $table = 'posts';
    protected $fillable = ['title', 'slug', 'intro', 'body', 'user_id']; //которые можно автоматически заполнять
    //protected $guarded = ['id']; //которые нельзя автоматически заполнять

    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
