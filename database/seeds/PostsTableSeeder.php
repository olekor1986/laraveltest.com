<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'title' => 'Php is awesome',
                'user_id' => 1,
                'slug' => 'php_is_awesome',
                'intro' => 'We love PHP! Lets enjoy',
                'body' => 'We love PHP! Lets enjoy We love PHP! Lets enjoy'
            ],
            [
                'title' => 'HTML is awesome',
                'user_id' => 1,
                'slug' => 'html_is_awesome',
                'intro' => 'We love HTML! Lets enjoy',
                'body' => 'We love HTML! Lets enjoy We love HTML! Lets enjoy'
            ],
            [
                'title' => 'CSS is awesome',
                'user_id' => 1,
                'slug' => 'css_is_awesome',
                'intro' => 'We love CSS! Lets enjoy',
                'body' => 'We love CSS! Lets enjoy We love CSS! Lets enjoy'
            ]
        ]);

    }
}
