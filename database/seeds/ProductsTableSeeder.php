<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::insert([
            [
                'title' => 'PHP course',
                'slug' => 'php_sourse',
                'price' => 10,
                'description' => 'Super PHP course'
            ],
            [
                'title' => 'JS course',
                'slug' => 'js_sourse',
                'price' => 11,
                'description' => 'Super JS course'
            ]]
        );
    }
}
