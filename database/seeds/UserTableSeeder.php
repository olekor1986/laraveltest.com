<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'name' => 'Dummy author',
                'email' => 'author@mail.com',
                'password' => Hash::make('MegaPassword')
            ]
        );
    }
}
