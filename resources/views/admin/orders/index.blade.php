@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h2 class="display-7">Orders:</h2>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        @foreach($orders as $order)
            <div class="col-md-12">
                <h4>#{{$order->id}} - {{$order->user->name}}</h4>
                <p>{{$order->phone}}</p>
                <p>{{$order->comment}}</p>
                <ul>
                @foreach($order->products as $product)
                    <li>
                        <strong>{{$product->id}}</strong>
                        {{$product->title}} x {{$product->pivot->amount}}
                    </li>
                    @endforeach
                </ul>
            </div>
        @endforeach
    </div>
@endsection
