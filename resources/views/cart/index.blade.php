@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h4 class="display-6">Cart</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <tr>
                        <th>Product title</th>
                        <th>Price</th>
                        <th>Amount</th>
                        <th>Total Price:</th>
                    </tr>
                @foreach($cartArray as $cartItem)
                        <tr>
                            <td>{{$cartItem['product']->id}} - {{$cartItem['product']->title}}</td>
                            <td>{{$cartItem['product']->price}}</td>
                            <td>
                            <a href="/cart/{{$cartItem['product']->slug}}/delete" class="btn btn-sm btn-danger">-</a>
                            {{$cartItem['amount']}}
                            <a href="/cart/{{$cartItem['product']->slug}}" class="btn btn-sm btn-success">+</a>
                            </td>
                            <td>{{round($cartItem['product']->price * $cartItem['amount'], 2)}}</td>
                        </tr>
                @endforeach
                </table>
                <p>
                    <strong><a href="/order" class="btn btn-primary" role="button">Create order</a></strong>
                </p>
            </div>
        </div>
        <hr>
    </div>
@endsection

