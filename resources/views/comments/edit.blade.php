@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Edit Comment:</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class ="form-horizontal" method="post" action="/comments/{{$comment->id}}">
                {{csrf_field()}}

                @method('put')
                <input type="hidden" name="id" value="{{$comment->id}}">
                <div class="form-group">
                    <label>Body:
                        <input type="text" name="body" class="form-control" value="{{$comment->body}}">
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection

