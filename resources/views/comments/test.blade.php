@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h4 class="display-6">test</h4>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <table class="table">
            <tr>
                <th>id</th>
                <th>post_id</th>
                <th>body</th>
                <th>author</th>
            </tr>

                @foreach($comments as $comment)
                <tr>
                    <td>{{$comment->id}}</td>
                    <td>{{$comment->post->title}}</td>
                    <td>{{$comment->body}}</td>
                    <td>{{$comment->user->name}}</td>
                </tr>
                @endforeach

        </table>
    </div>
@endsection


