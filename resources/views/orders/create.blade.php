@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Your order:</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form class ="form-horizontal" method="post" action="/order">
                {{csrf_field()}}

                <div class="form-group">
                    <ul>
                        @foreach($cartWithProducts as $cartItem)
                            <li>
                                {{$cartItem['product']->title}} x {{$cartItem['amount']}}
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="form-group">
                    <label>Phone:
                        <input type="text" name="phone" class="form-control">
                    </label>
                </div>
                <div class="form-group">
                    <label>Comment:
                        <textarea name="comment" class="form-control"></textarea>
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">publish Order</button>
                </div>
            </form>
        </div>
    </div>
@endsection
