@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Edit blog post:</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class ="form-horizontal" method="post" action="/posts/{{$post->slug}}">
                {{csrf_field()}}

                @method('put')
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="form-group">
                    <label>Title:
                        <input type="text" name="title" class="form-control" value="{{$post->title}}">
                    </label>
                </div>
                <div class="form-group">
                    <label>Slug:
                        <input type="text" name="slug" class="form-control" value="{{$post->slug}}">
                    </label>
                </div>
                <div class="form-group">
                    <label>Intro:
                        <input type="text" name="intro" class="form-control" value="{{$post->intro}}">
                    </label>
                </div>
                <div class="form-group">
                    <label>Body:
                        <textarea name="body" class="form-control">{{$post->body}}</textarea>
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
