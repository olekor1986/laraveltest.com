@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">{{$pageTitle}}</h1>
            <p>Read with pleasure!</p>

        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            @foreach($posts as $post)
            <div class="col-md-4">
                <h2>{{$post->title}}</h2>
               <p><strong>{{$post->user->name}}</strong></p>
                <p>{{$post->body}}</p>
                <p><a class="btn btn-secondary" href="/posts/{{$post->slug}}" role="button">View details &raquo;</a></p>
                @if(Auth::check())
                <p><a class="btn btn-primary" href="/posts/{{$post->slug}}/edit" role="button">Edit post &raquo;</a></p>
                <form action="/posts/{{$post->slug}}" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger">Delete</button>
                </form>
                @endif
            </div>
            @endforeach
        </div>
        <hr>
    </div>
@endsection

{{--<main role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Hello, world!</h1>
            <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>

        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-4">
                <h2>Heading</h2>
                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
            </div>

        </div>

        <hr>

    </div> <!-- /container -->

</main>--}}

