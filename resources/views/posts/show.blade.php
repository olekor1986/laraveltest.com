@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">{{$post->title}}</h1>
            <p>{{$post->intro}}</p>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-4">
                <p>{{$post->body}}</p>
            </div>
        </div>
        <hr>
        <h2>Comments</h2>
        <div class="col-md-12">
            <ul class="list-group">
                @foreach($post->comments as $comment)
                    <li class="list-group item">
                    {{--    {{$comment->created_at->diffForHumans()}} --}}
                        {{$comment->user->name}}
                       <strong>{{$comment->body}}</strong>
                        @if(Auth::check())
                        <p><a class="btn btn-success" href="/comments/{{$comment->id}}/edit">Edit</a></p>
                        <form action="/comments/{{$comment->id}}" method="post">
                            @method('delete')
                            @csrf
                            <button class="btn btn-danger">X</button>
                        </form>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
        @if(Auth::check())
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/comments">
                    @csrf
                    <input type="hidden" name="post_id" value="{{$post->id}}">

                    <div class="form-group">
                        <label>Comment:
                            <textarea name="body"  class="form-control"></textarea>
                        </label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn-btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
        @endif
    </div>
@endsection

{{--<main role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Hello, world!</h1>
            <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>

        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-4">
                <h2>Heading</h2>
                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
            </div>

        </div>

        <hr>

    </div> <!-- /container -->

</main>--}}

