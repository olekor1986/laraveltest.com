@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h4 class="display-6">Products</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @foreach($products as $product)
            <div class="col-md-4">
                <h2>{{$product->title}}</h2>
                <p>{{$product->price}}</p>
                <p><a class="btn btn-secondary" href="/products/{{$product->slug}}" role="button">Details</a></p>
                @if(Auth::check())
                    <a class="btn btn-success" href="/cart/{{$product->slug}}" role="button">Buy</a>
                @endif
            </div>
            @endforeach
        </div>
        <hr>
    </div>
@endsection

